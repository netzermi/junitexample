import at.hakimst.apr.Kommentar;
import org.junit.Before;
import org.junit.Test;

import java.beans.Transient;

import static org.junit.Assert.assertEquals;

public class TestKommentar {
    Kommentar k1;
    Kommentar k2;

    @Before
    public void prepare(){
        k1 = new Kommentar("Max","Hallo");
        k2 = new Kommentar("Max", "Hallo");
    }

    @Test
    public void testEquals(){
        assertEquals(true, k1.equals(k2));
    }

    @Test
    public void testLike(){
        k1.like();
        k1.like();
        assertEquals(2,k1.getStimmen());
    }

    @Test
    public void testDurchschnittsBewertung(){
        k1.like();
        k1.like();
        assertEquals(4.900, k1.getDurchschnittsBewertung(),1E-1);
    }
}

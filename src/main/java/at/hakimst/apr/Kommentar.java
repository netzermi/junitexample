package at.hakimst.apr;

import java.util.Objects;

public class Kommentar {
    private String autor;
    private String text;
    private int stimmen;
    private double durchschnittsBewertung;


    public Kommentar(String autor, String text){
        this.autor = autor;
        this.text = text;
        this.stimmen = 0;
    }

    public void like(){
        stimmen++;
        durchschnittsBewertung = stimmen/5d;
    }

    public String getAutor() {
        return autor;
    }

    public int getStimmen() {
        return stimmen;
    }

    public String getText() {
        return text;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setStimmen(int stimmen) {
        this.stimmen = stimmen;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getDurchschnittsBewertung() {
        return 4.99;
    }

    //k1.equals(k2);
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kommentar kommentar = (Kommentar) o;
        return stimmen == kommentar.stimmen && Objects.equals(autor, kommentar.autor) && Objects.equals(text, kommentar.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(autor, text, stimmen);
    }


}
